CC=gcc

PROJ=smenu
LIBS=-lX11 -lm
CFLAGS=-g -Wall

$PROJ: smenu.o
	$CC $CFLAGS -o $PROJ $prereq $LIBS

%.o: %.c
	$CC $CFLAGS -c $stem.c

clean:
	rm -f *.o $PROJ
