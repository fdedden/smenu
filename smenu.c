#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <X11/Xlib.h>
#include <X11/keysym.h>

#define LEN(x) (sizeof(x)/sizeof(x[0]))

#define LINE_HEIGHT 20
#define MAX_DRAW 10

#define PROMPT "prompt: "

typedef struct Item Item;
typedef struct Itemlist Itemlist;
struct Item
{
	char *text;
	Item *prev, *next;
};
struct Itemlist
{
	Item *first, *last;
};


Display *dpy;
Window  win;
int     screen;
GC      gc;
XIC     xic;
XIM     xim;

Itemlist readItems(FILE *fp) {
	Itemlist il = {NULL, NULL};
	Item *prev = NULL;
	char buff[BUFSIZ];

	while(fgets(buff, sizeof(buff), fp) != NULL) {
		/* Replace \n with \0 */
		char *p = NULL;
		if((p = strchr(buff, '\n'))) {
			*p = '\0';
		}

		/* Copy the text to a new item */
		Item *i = malloc(sizeof(Item));
		i->text = malloc(sizeof(char)*sizeof(buff));
		strcpy(i->text, buff);

		/* Update pointers */
		i->prev = prev;
		i->next = NULL;
		if(prev) {
			prev->next = i;
		}
		if(!il.first) {
			il.first = i;
		}
		prev = i;
	}
	il.last = prev;

	return il;
}

void freeItems(Itemlist il) {
	Item *p = il.first;
	while(p) {
		Item *tmp = p;
		p = p->next;
		if(tmp) {
			free(tmp->text);
			free(tmp);
		}
	}
}


void renderItem(int x, int y, Item *i, int curr)
{
	if(curr) {
		XFillRectangle(dpy, win, gc, x, y, LINE_HEIGHT, LINE_HEIGHT);
	}
	XDrawString(dpy, win, gc, x + LINE_HEIGHT, y + LINE_HEIGHT / 2, i->text, strlen(i->text));
}


void renderList(Itemlist items, Item *sel)
{
	int y = LINE_HEIGHT;
	int curr = 0;
	Item *p = items.first;
	while(p) {
		if(p == sel) {
			curr = 1;
		}
		else {
			curr = 0;
		}
		renderItem(0, y, p, curr);
		y += LINE_HEIGHT;
		p = p->next;
	}
}


void renderPrompt(const char* prompt, const char* text)
{
	char buff[strlen(prompt) + strlen(text)];

	strcpy(buff, PROMPT);
	strcat(buff, text);

	XDrawString(dpy, win, gc, 0, LINE_HEIGHT / 2, buff, strlen(buff));
}



void setupx() {
	if(!(dpy = XOpenDisplay(NULL))) {
		fprintf(stderr, "Cannot open display\n");
		exit(1);
	}
	screen = DefaultScreen(dpy);
	win = XCreateSimpleWindow(dpy,
		RootWindow(dpy, screen),
		0, 0,       /* coordinates */
		500, 500,   /* width / height */
		1,          /* border */
		BlackPixel(dpy, screen),
		WhitePixel(dpy, screen)
	);

	gc = DefaultGC(dpy, screen);

	/* TODO: what does this exactly? */
	xim = XOpenIM(dpy, NULL, NULL, NULL);
	xic = XCreateIC(xim, XNInputStyle, XIMPreeditNothing | XIMStatusNothing,
	                XNClientWindow, win, XNFocusWindow, win, NULL);

	XSelectInput(dpy, win, ExposureMask | KeyPressMask | ButtonPressMask);
	XGrabButton(dpy, AnyButton, AnyModifier, DefaultRootWindow(dpy), True, 0, GrabModeAsync, GrabModeAsync, None, None);
	XMapRaised(dpy, win);
}


int main(int argc, char* argv[])
{
	XEvent  evt;

	Itemlist il;
	Item    *selected;

	int running = 1;

	char    inputline[BUFSIZ];
	int     cursor = 0;

	/* Disable stdout buffering, so printing immediately takes effect */
	setbuf(stdout, NULL);

	setupx();

	/* Read items from stdin */
	il = readItems(stdin);
	selected = il.first;

	while(running) {
		XNextEvent(dpy, &evt);

		if(evt.type == ButtonPress) {
			switch(evt.xbutton.button) {
				case Button1: /* apply */
					printf("%s\n", selected->text);
					running = 0;
					break;
				case Button3: /* abort */
					running = 0;
					break;
				case Button4: /* scroll up */
					selected = selected->prev;
					if(!selected) {
						selected = il.first;
					}
					break;
				case Button5: /* scroll down */
					selected = selected->next;
					if(!selected) {
						selected = il.last;
					}
					break;
			}
		}

		if(evt.type == KeyPress) {
			char buff[32];
			KeySym sym;
			Status stat;
			XmbLookupString(xic, &evt.xkey, buff, sizeof(buff)*sizeof(char), &sym, &stat);

			switch(sym) {
				default:
					if(strlen(buff)) {
						strcat(inputline, buff);
						cursor = (cursor+1) % strlen(inputline);
						printf("%s\n", inputline);
					}
					break;
				case XK_BackSpace:
					cursor = fmax((cursor - 1), 0);
					inputline[cursor] = '\0';
					printf("%s\n", inputline);
					break;
			}
		}

		/* Just redraw everyting at each event */
		XClearWindow(dpy, win);
		renderPrompt(PROMPT, inputline);
		renderList(il, selected);
	}

	XDestroyIC(xic);
	XCloseIM(xim);
	XCloseDisplay(dpy);

	freeItems(il);

	return 0;
}
